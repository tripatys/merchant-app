/*******************************************************************************
 * Copyright (c) 2017, MeaWallet AS. All rights reserved.
 *******************************************************************************/

package com.meawallet.merchantapp.model;

import org.json.JSONException;
import org.json.JSONObject;

import flexjson.JSON;

public class TransactionResponse {

    private static final String KEY_ATC = "atc";
    private static final String KEY_CRYPTOGRAM = "cryptogram";
    private static final String KEY_CRYPTOGRAM_TYPE = "cryptogramType";
    private static final String KEY_CURRENCY_CODE = "currencyCode";
    private static final String KEY_EXPIRY_DATE = "expiryDate";
    private static final String KEY_EXPIRY_DAY = "day";
    private static final String KEY_EXPIRY_MONTH = "month";
    private static final String KEY_EXPIRY_VALID = "valid";
    private static final String KEY_EXPIRY_YEAR = "year";
    private static final String KEY_PAN = "pan";
    private static final String KEY_PAN_SEQUENCE_NUMBER = "panSequenceNumber";
    private static final String KEY_TRANSACTION_AMOUNT = "transactionAmount";
    private static final String KEY_TRANSACTION_CRYPTOGRAM_DATA = "transactionCryptogramData";
    private static final String KEY_UCAF_VERSION = "ucafVersion";
    private static final String KEY_UNPREDICTABLE_NUMBER = "unpredictableNumber";

    @JSON(name = "atc")
    private int mApplicationTransactionCounter;

    @JSON(name = "cryptogram")
    private String mCryptogram;

    @JSON(name = "cryptogramType")
    private String mCryptogramType;

    @JSON(name = "currencyCode")
    private int mCurrencyCode;

    @JSON(name = "expiryDate")
    private ExpiryDate mExpiryDate;

    @JSON(name = "pan")
    private String mPan;

    @JSON(name = "panSequenceNumber")
    private int mPanSequenceNumber;

    @JSON(name = "transactionAmount")
    private int mTransactionAmount;

    @JSON(name = "transactionCryptogramData")
    private String mTransactionCryptogramData;

    @JSON(name = "ucafVersion")
    private String ucafVersion;

    @JSON(name = "unpredictableNumber")
    private String mUnpredictableNumber;

    public class ExpiryDate {

        @JSON(name = "day")
        public int mDay;

        @JSON(name = "month")
        public int mMonth;

        @JSON(name = "year")
        public int mYear;

        @JSON(name = "valid")
        public boolean mValid;
    }

    public TransactionResponse(String json) {
        mExpiryDate = new ExpiryDate();

        try {
            JSONObject obj = new JSONObject(json);

            if (obj.has(KEY_ATC)) {
                mApplicationTransactionCounter = obj.getInt(KEY_ATC);
            }

            if (obj.has(KEY_CRYPTOGRAM)) {
                mCryptogram = obj.getString(KEY_CRYPTOGRAM);
            }

            if (obj.has(KEY_CRYPTOGRAM_TYPE)) {
                mCryptogramType = obj.getString(KEY_CRYPTOGRAM_TYPE);
            }

            if (obj.has(KEY_CURRENCY_CODE)) {
                mCurrencyCode = obj.getInt(KEY_CURRENCY_CODE);
            }

            if (obj.has(KEY_EXPIRY_DATE)) {
                JSONObject expiryObj = obj.getJSONObject(KEY_EXPIRY_DATE);

                if (expiryObj.has(KEY_EXPIRY_DAY)) {
                    mExpiryDate.mDay = expiryObj.getInt(KEY_EXPIRY_DAY);
                }

                if (expiryObj.has(KEY_EXPIRY_MONTH)) {
                    mExpiryDate.mMonth = expiryObj.getInt(KEY_EXPIRY_MONTH);
                }

                if (expiryObj.has(KEY_EXPIRY_YEAR)) {
                    mExpiryDate.mYear = expiryObj.getInt(KEY_EXPIRY_YEAR);
                }
            }

            if (obj.has(KEY_PAN)) {
                mPan = obj.getString(KEY_PAN);
            }

            if (obj.has(KEY_PAN_SEQUENCE_NUMBER)) {
                mPanSequenceNumber = obj.getInt(KEY_PAN_SEQUENCE_NUMBER);
            }

            if (obj.has(KEY_TRANSACTION_AMOUNT)) {
                mTransactionAmount = obj.getInt(KEY_TRANSACTION_AMOUNT);
            }

            if (obj.has(KEY_TRANSACTION_CRYPTOGRAM_DATA)) {
                mTransactionCryptogramData = obj.getString(KEY_TRANSACTION_CRYPTOGRAM_DATA);
            }

            if (obj.has(KEY_UCAF_VERSION)) {
                ucafVersion = obj.getString(KEY_UCAF_VERSION);
            }

            if (obj.has(KEY_UNPREDICTABLE_NUMBER)) {
                mUnpredictableNumber = obj.getString(KEY_UNPREDICTABLE_NUMBER);
            }
        }
        catch (JSONException ex) {
            ex.printStackTrace();
        }
    }
}
