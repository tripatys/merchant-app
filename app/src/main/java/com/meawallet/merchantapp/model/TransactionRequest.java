/*******************************************************************************
 * Copyright (c) 2017, MeaWallet AS. All rights reserved.
 *******************************************************************************/

package com.meawallet.merchantapp.model;

import java.util.Date;

import flexjson.JSON;

public class TransactionRequest {

    private static final String CRYPTOGRAM_TYPE_UCAF = "UCAF";
    private static final String CRYPTOGRAM_TYPE_DE55 = "DE55";

    @JSON(name = "transactionAmount")
    private int mTransactionAmount;

    @JSON(name = "otherAmount")
    private int mOtherAmount;

    @JSON(name = "currency")
    private int mCurrency;

    @JSON(name = "transactionType")
    private int mTransactionType;

    @JSON(name = "unpredictableNumber")
    private int mUnpredictableNumber;

    @JSON(name = "cryptogramType")
    private String mCryptogramType;

    @JSON(name = "merchantLogo")
    private String mMerchantLogo;

    @JSON(name = "countryCode")
    private int mCountryCode;

    @JSON(name = "merchant")
    private String mMerchant;

    @JSON(name = "inApp")
    private boolean mInApp;

    @JSON(name = "transactionDay")
    private int mTransactionDay;

    @JSON(name = "transactionMonth")
    private int mTransactionMonth;

    @JSON(name = "transactionYear")
    private int mTransactionYear;

    public static String getCryptogramTypeUcaf() {
        return CRYPTOGRAM_TYPE_UCAF;
    }

    public static String getCryptogramTypeDe55() {
        return CRYPTOGRAM_TYPE_DE55;
    }

    public int getTransactionAmount() {
        return mTransactionAmount;
    }

    public void setTransactionAmount(int transactionAmount) {
        mTransactionAmount = transactionAmount;
    }

    public int getOtherAmount() {
        return mOtherAmount;
    }

    public void setOtherAmount(int otherAmount) {
        mOtherAmount = otherAmount;
    }

    public int getCurrency() {
        return mCurrency;
    }

    public void setCurrency(int currency) {
        mCurrency = currency;
    }

    public int getTransactionType() {
        return mTransactionType;
    }

    public void setTransactionType(int transactionType) {
        mTransactionType = transactionType;
    }

    public int getUnpredictableNumber() {
        return mUnpredictableNumber;
    }

    public void setUnpredictableNumber(int unpredictableNumber) {
        mUnpredictableNumber = unpredictableNumber;
    }

    public void setCryptogramType(String cryptogramType) {
        mCryptogramType = cryptogramType;
    }

    public String getMerchantLogo() {
        return mMerchantLogo;
    }

    public void setMerchantLogo(String merchantLogo) {
        mMerchantLogo = merchantLogo;
    }

    public int getCountryCode() {
        return mCountryCode;
    }

    public void setCountryCode(int countryCode) {
        mCountryCode = countryCode;
    }

    public String getMerchant() {
        return mMerchant;
    }

    public void setMerchant(String merchant) {
        mMerchant = merchant;
    }

    public boolean ismInApp() {
        return mInApp;
    }

    public void setInApp(boolean inApp) {
        mInApp = inApp;
    }

    public int getTransactionDay() {
        return mTransactionDay;
    }

    public void setTransactionDay(int transactionDay) {
        mTransactionDay = transactionDay;
    }

    public int getTransactionMonth() {
        return mTransactionMonth;
    }

    public void setTransactionMonth(int transactionMonth) {
        mTransactionMonth = transactionMonth;
    }

    public int getTransactionYear() {
        return mTransactionYear;
    }

    public void setTransactionYear(int transactionYear) {
        mTransactionYear = transactionYear;
    }

    public TransactionRequest(int amount,
                              String merchantLogo,
                              int currency,
                              Date transactionDate,
                              String merchant,
                              boolean inApp) {
        mTransactionAmount = amount;
        mMerchantLogo = merchantLogo;
        mCurrency = currency;
        mTransactionDay = transactionDate.getDay();
        mTransactionMonth = transactionDate.getMonth();
        mTransactionYear = transactionDate.getYear();
        mMerchant = merchant;
        mInApp = inApp;
    }

    public enum CryptogramType {
        UCAF,
        DE55
    }

    public void setCryptogramType(CryptogramType cryptogramType) {
        switch (cryptogramType) {
            case DE55:
                mCryptogramType = CRYPTOGRAM_TYPE_DE55;
                break;
            default:
            case UCAF:
                mCryptogramType = CRYPTOGRAM_TYPE_UCAF;
                break;
        }
    }

    public CryptogramType getCryptogramType() {

        if (mCryptogramType != null) {
            if (mCryptogramType.equals(CRYPTOGRAM_TYPE_DE55)) {
                return CryptogramType.DE55;
            } else {
                return CryptogramType.UCAF;
            }
        } else {
            return CryptogramType.UCAF;
        }
    }
}
