/*******************************************************************************
 * Copyright (c) 2017, MeaWallet AS. All rights reserved.
 *******************************************************************************/

package com.meawallet.merchantapp.interfaces;

import static android.R.attr.name;

public interface Constants {

    String MERCHANT_LOGO_URL = "merchant_logo_url";
    String MERCHANT_NAME = "CWD Merchant";
    int CURRENCY_CODE = 978;
    int REQUEST_CODE_FOR_PAYMENT = 123;
    String INTENT_KEY_RECEIVER = "dsrp";
    String BLANK = "";
    String UTF = "UTF-8";
    String DEEP_LINK_URL = "payment://order/?data=";
}