package com.meawallet.merchantapp;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class HomeScreen extends BaseActivity {

    private EditText mEditDonate;
    private Button mButtonDonate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);
        initializeUI();
    }

    /**
     * Initialize all UI component for Later Use.
     */
    public void initializeUI() {
        mEditDonate = (EditText) findViewById(R.id.edit_donate);
        mButtonDonate = (Button) findViewById(R.id.button_donate);
        mButtonDonate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (validate()) {
                    String url = getString(R.string.checkout_deep_link_scheme) + "://" + getString(R.string.deep_link_url) + getString(R.string.checkout_deep_link_prefix) + "?" + DONATION_AMOUNT + "=" + mEditDonate.getText().toString();
                    Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse(url));
                    startActivity(intent);
                }
            }
        });
    }

    public boolean validate() {

        if (mEditDonate.getText().toString().trim().length() == 0) {
            Toast.makeText(getApplicationContext(), "Please enter donation amount", Toast.LENGTH_SHORT).show();

            return false;
        }

        return true;
    }

}
