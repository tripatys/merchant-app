/*******************************************************************************
 * Copyright (c) 2017, MeaWallet AS. All rights reserved.
 *******************************************************************************/

package com.meawallet.merchantapp.ui;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.EditText;

import com.meawallet.merchantapp.R;
import com.meawallet.merchantapp.model.TransactionRequest;
import com.meawallet.merchantapp.util.Utility;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Calendar;
import java.util.Date;

import flexjson.JSONSerializer;

public class HomeScreenActivity extends BaseActivity {

    private EditText mEditTextEnterAmount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);
        initializeUI();
    }

    private void initializeUI() {
        mEditTextEnterAmount = (EditText) findViewById(R.id.edit_donate);
        findViewById(R.id.button_donate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (validate()) {
                    sendPaymentIntent(Integer.valueOf(mEditTextEnterAmount.getText().toString()));
                }
            }
        });
    }

    private boolean validate() {

        if (mEditTextEnterAmount.getText().toString().trim().length() == 0) {
            Snackbar.make(findViewById(R.id.layout_parent), getString(R.string.validate_donation_amount), Snackbar.LENGTH_SHORT).show();

            return false;
        }

        return true;
    }

    private void sendPaymentIntent(int amountToPay) {
        Calendar calendar = Calendar.getInstance();
        Date transactionDate = new Date(calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH) + 1,
                calendar.get(Calendar.DAY_OF_MONTH));
        TransactionRequest transactionRequest = new TransactionRequest(amountToPay, MERCHANT_LOGO_URL,
                CURRENCY_CODE, transactionDate, MERCHANT_NAME, true);
        transactionRequest.setCryptogramType(TransactionRequest.CryptogramType.DE55);
        String masterPassPayloadJson = null;

        try {
            masterPassPayloadJson = URLEncoder.encode(new JSONSerializer().exclude("class").serialize(transactionRequest),UTF);
        } catch (UnsupportedEncodingException e) {
            masterPassPayloadJson = BLANK;
        }
        
        Intent paymentIntent = new Intent();
        paymentIntent.setAction(Intent.ACTION_VIEW);
        paymentIntent.setData(Uri.parse(DEEP_LINK_URL + masterPassPayloadJson));
        startActivityForResult(paymentIntent, REQUEST_CODE_FOR_PAYMENT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if (intent != null && resultCode == RESULT_OK && requestCode == REQUEST_CODE_FOR_PAYMENT) {
            Utility.alert(this, getString(R.string.payment_success));

            if (intent.getStringExtra(INTENT_KEY_RECEIVER) != null && intent.getStringExtra(INTENT_KEY_RECEIVER).length() > 0) {
                // TODO Display the required data in UI and check for authorization and Issuer Approval with Server
//                TransactionResponse transactionResponse = new TransactionResponse(URLDecoder
//                      .decode(intent.getStringExtra(INTENT_KEY_RECEIVER)));
            }
        } else if (requestCode == REQUEST_CODE_FOR_PAYMENT && resultCode == RESULT_CANCELED) {
            Snackbar.make(findViewById(R.id.layout_parent), getString(R.string.payment_cancelled), Snackbar.LENGTH_LONG)
                    .setAction(R.string.retry, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                        }
                    }).setActionTextColor(Color.RED).show();
        }
    }
}