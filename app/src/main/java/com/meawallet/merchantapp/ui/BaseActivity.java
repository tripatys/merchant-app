/*******************************************************************************
 * Copyright (c) 2017, MeaWallet AS. All rights reserved.
 *******************************************************************************/

package com.meawallet.merchantapp.ui;

import android.support.v7.app.AppCompatActivity;

import com.meawallet.merchantapp.interfaces.Constants;


public class BaseActivity extends AppCompatActivity implements Constants {
}
