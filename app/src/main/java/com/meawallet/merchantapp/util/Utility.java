/*******************************************************************************
 * Copyright (c) 2017, MeaWallet AS. All rights reserved.
 *******************************************************************************/

package com.meawallet.merchantapp.util;

import android.app.Activity;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import com.meawallet.merchantapp.R;

public class Utility {

    public static void alert(final Activity activity, final String message) {

        if (!activity.isFinishing()) {
            activity.runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    AlertDialog.Builder alertBox = new AlertDialog.Builder(activity);
                    alertBox.setTitle(R.string.app_name);
                    alertBox.setMessage(message);
                    alertBox.setCancelable(true);
                    alertBox.setPositiveButton(R.string.alert_ok, new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface arg0, int arg1) {
                            arg0.dismiss();
                        }
                    });
                    alertBox.show();
                }
            });
        }
    }
}
